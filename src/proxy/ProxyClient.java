package proxy;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import proxy.logger.ProxyLogger;

public class ProxyClient {

	private String baseUrl;
	private String basePath;
	private String protocol;
	private int port;

	public ProxyClient(String protocol, String baseUrl, String basePath, int port) {
		this.baseUrl = baseUrl;
		this.protocol = protocol;
		this.port = port;
		this.basePath = basePath;
	}

	public Connection connect(String path, String pathArgs, String method) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		while (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		while (path.startsWith("/")) {
			path = (path.length() == 1) ? "" : path.substring(1);
		}
		String urlString = protocol + "://" + baseUrl + ":" + port + ((basePath.length() > 0) ? "/" + basePath : "") + ((path.length() > 0) ? "/" + path : "") + ((pathArgs.length() > 0) ? "?" + pathArgs : "");
		URL url = new URL(urlString);
		ProxyLogger.info("Redirected to: " + urlString + " -> " + method);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod(method);
		con.setDoInput(true);
		if (!"GET".equalsIgnoreCase(method.trim())) con.setDoOutput(true);
		ProxyLogger.debug("DoOutput: " + con.getDoOutput());		
		return new Connection(con);
	}

	@Override
	public String toString() {
		return "ProxyClient [baseUrl=" + baseUrl + ", basePath=" + basePath + ", protocol=" + protocol + ", port="
				+ port + "]";
	}
}
