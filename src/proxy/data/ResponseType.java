package proxy.data;

public enum ResponseType {
	
	OK(200),
	CREATED(201),
	ACCEPTED(202),
	NON_AUTHORITATIVE_INFORMATION(203),
	NO_CONTENT(204),
	RESET_CONTENT(205),
	PARTIAL_CONTENT(206),
	MULTI_STATUS(206),
	BAD_REQUEST(400),
	UNAUTHORIZED(401),
	FORBIDDEN(403),
	NOT_FOUND(404),
	METHOD_NOT_ALLOWED(405),
	INTERNAL_SERVER_ERROR(500);

	private int statusCode;

	private ResponseType(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public static boolean isOK(int responseCode) {
		return (responseCode + "").charAt(0) == '2';
	}
}
