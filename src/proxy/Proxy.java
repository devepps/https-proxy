package proxy;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class Proxy {

	public static void main(String[] args) throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException {
		new Proxy();
	}
	
	private Server server;

	public Proxy() throws UnrecoverableKeyException, KeyManagementException, NoSuchAlgorithmException, CertificateException, KeyStoreException, IOException {
		this.server = new Server("proxy.port", "proxy.ssl-keystore.path", "proxy.ssl-keystore.password");
		this.server.addContext("/", new HTTPRequestHandler());
		this.server.start();
	}

}
