package proxy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.sun.net.httpserver.Headers;

public class Connection {

	private class MyManager implements X509TrustManager {
		@Override
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		}

		@Override
		public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	}

	private HttpURLConnection con;

	@SuppressWarnings("static-access")
	public Connection(HttpURLConnection con) throws NoSuchAlgorithmException, KeyManagementException {
		super();
		this.con = con;

		if (con instanceof HttpsURLConnection) {
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new TrustManager[] { new MyManager() }, null);
			((HttpsURLConnection) this.con).setSSLSocketFactory(sslContext.getSocketFactory());
			((HttpsURLConnection) this.con).setDefaultHostnameVerifier((hostname, session) -> true);
		}
	}

	public void addBody(InputStream data) throws IOException {
		if (!con.getDoOutput()) {
			data.close();
			return;
		}
		byte[] buffer = new byte[1024 * 1024];
		for (int bytesRead = data.read(buffer); bytesRead != -1; bytesRead = data.read(buffer)) {
			if (bytesRead == 0) return;
			con.getOutputStream().write(buffer, 0, bytesRead);
		}
		data.close();
	}

	public int send() throws IOException {
		if (con.getDoOutput()) {
			con.getOutputStream().close();
		}
		return con.getResponseCode();
	}

	public void addHeader(String headerKey, String... headerContents) {
		for (String content : headerContents) {
			con.addRequestProperty(headerKey, content);
		}
	}

	public void addRequestHeaders(Headers requestHeaders) {
		for (String headerKey : requestHeaders.keySet()) {
			for (String content : requestHeaders.get(headerKey)) {
				con.addRequestProperty(headerKey, content);
			}
		}
	}

	public long writeBody(OutputStream out, boolean isOk) throws IOException {
		long length = 0;
		byte[] buffer = new byte[1024 * 1024];
		InputStream in = isOk ? con.getInputStream() : con.getErrorStream();
		for (int bytesRead = in.read(buffer); bytesRead > 0; bytesRead = in.read(buffer)) {
			if (bytesRead == 0) break;
			out.write(buffer, 0, bytesRead);
			length += bytesRead;
		}
		in.close();
		return length;
	}

	public void transferResponseHeaders(Headers responseHeaders) {
		for (String headerKey : con.getHeaderFields().keySet()) {
			if (headerKey != null) responseHeaders.put(headerKey, con.getHeaderFields().get(headerKey));
		}
	}

	public long getResponseContentLength() {
		List<String> contentLength = con.getHeaderFields().get("Content-Length");
		return (contentLength == null || contentLength.size() == 0) ? 0 : Long.parseLong(contentLength.get(0));
	}

	public void disableBody() {
		con.setDoOutput(false);
	}

}
