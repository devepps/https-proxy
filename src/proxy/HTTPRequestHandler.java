package proxy;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import collection.sync.SyncHashMap;
import config.ConfigEnvironment;
import proxy.data.ResponseType;
import proxy.logger.ProxyLogger;

public class HTTPRequestHandler implements HttpHandler {

	private static final String PROXY_HEADER_KEY_IP = ConfigEnvironment.getProperty("proxy.header-key.ip");

	private final SyncHashMap<String, ProxyClient> connections = new SyncHashMap<>();

	public HTTPRequestHandler() {
		for (String key : ConfigEnvironment.getSubProperties("proxy.connections")) {
			this.connections.put(
					ConfigEnvironment.getProperty("proxy.connections" + "." + key + ".path-key").toLowerCase(),
					new ProxyClient(ConfigEnvironment.getProperty("proxy.connections" + "." + key + ".protocol"),
							ConfigEnvironment.getProperty("proxy.connections" + "." + key + ".url"),
							ConfigEnvironment.getProperty("proxy.connections" + "." + key + ".basePath"),
							Integer.parseInt(ConfigEnvironment.getProperty("proxy.connections" + "." + key + ".port"))));
			ProxyLogger.info("Connection \"" + key + "\" registered as \"" + ConfigEnvironment.getProperty("proxy.connections" + "." + key + ".path-key").toLowerCase() + "\"");
		}
	}

	@Override
	public void handle(HttpExchange http) {
		ProxyLogger.info("New Request: " + http.getRequestURI() + " -> " + http.getRequestMethod());
		try {
			Connection con = buildProxyConnection(http);
			if (con == null) {
				ProxyLogger.debug("No proxied server found!");
				http.sendResponseHeaders(ResponseType.NOT_FOUND.getStatusCode(), 0);
				http.getResponseBody().close();
				return;
			}
			con.addRequestHeaders(http.getRequestHeaders());
			con.addHeader(PROXY_HEADER_KEY_IP, http.getRemoteAddress().getAddress().getHostAddress());
			List<String> contentLength = http.getRequestHeaders().get("Content-Length");
			long requestBodyLength = (contentLength == null || contentLength.size() == 0) ? 0 : Long.parseLong(contentLength.get(0));
			if (requestBodyLength > 0) {
				con.addBody(http.getRequestBody());
			} else {
				con.disableBody();
			}
			int responseCode = con.send();
			ProxyLogger.info("Request send to server");
			
			con.transferResponseHeaders(http.getResponseHeaders());
			http.sendResponseHeaders(responseCode, con.getResponseContentLength());
			con.writeBody(http.getResponseBody(), ResponseType.isOK(responseCode));
			http.getResponseBody().close();
			ProxyLogger.info("Request answered");
			http.close();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				http.sendResponseHeaders(ResponseType.INTERNAL_SERVER_ERROR.getStatusCode(), 0);
				http.getResponseBody().close();
				http.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private Connection buildProxyConnection(HttpExchange http) throws IOException, KeyManagementException, NoSuchAlgorithmException {
		String path = http.getRequestURI().getPath();
		path = path.startsWith("/") ? (path.length() == 1 ? "" : path.substring(1)) : path;
		String pathArgs = http.getRequestURI().toString().indexOf('?') == -1 ? "" : http.getRequestURI().toString().substring(http.getRequestURI().toString().indexOf('?') + 1);
		ProxyClient proxyClient = null;
		ProxyLogger.debug("Incoming URL: \"" + path + "\" Start factoring");
		for (String subPath = path.toLowerCase(); subPath.length() > 0; subPath = subPath.contains("/") ? subPath.substring(0, subPath.lastIndexOf("/")) : "") {
			ProxyLogger.debug("current subPath: " + subPath);
			if (connections.containsKey(subPath)) {
				proxyClient = connections.get(subPath);
				path = path.substring(subPath.length());
				break;
			}
		}
		ProxyLogger.debug("factored path: \"" + path + "\" -> " + proxyClient);
		proxyClient = (proxyClient == null) ? connections.get("") : proxyClient;
		return (proxyClient == null) ? null : proxyClient.connect(path, (pathArgs == null) ? "" : pathArgs, http.getRequestMethod());
	}

}
