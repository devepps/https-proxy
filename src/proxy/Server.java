package proxy;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import config.ConfigEnvironment;
import proxy.logger.ProxyLogger;

public class Server {

	private HttpsServer httpsServer;

	public Server(String configPort, String configPathToFile, String configFilePassword)
			throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException,
			UnrecoverableKeyException, KeyManagementException {

		this.httpsServer = HttpsServer.create(new InetSocketAddress(ConfigEnvironment.getPropertyInt(configPort)), 10);

		String password = ConfigEnvironment.getProperty(configFilePassword);
		SSLContext sslContext = SSLContext.getInstance("TLS");

		KeyStore ks = KeyStore.getInstance("JKS");
		FileInputStream fis = new FileInputStream(ConfigEnvironment.getProperty(configPathToFile));
		ks.load(fis, password.toCharArray());

		KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
		kmf.init(ks, password.toCharArray());

		TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
		tmf.init(ks);

		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
		httpsServer.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
			public void configure(HttpsParameters params) {
				try {
					// Initialise the SSL context
					SSLContext c = SSLContext.getDefault();
					SSLEngine engine = c.createSSLEngine();
					params.setNeedClientAuth(false);
					params.setCipherSuites(engine.getEnabledCipherSuites());
					params.setProtocols(engine.getEnabledProtocols());

					// Get the default parameters
					SSLParameters defaultSSLParameters = c.getDefaultSSLParameters();
					params.setSSLParameters(defaultSSLParameters);
				} catch (Exception e) {
					ProxyLogger.debug("Failed to create HTTPS port", e);
				}
			}
		});
	}

	public void addContext(String path, HttpHandler httpHandler) {
		this.httpsServer.createContext(path, httpHandler);
	}

	public void start() {
		httpsServer.setExecutor(Executors.newCachedThreadPool());
		httpsServer.start();
		ProxyLogger.info("Server started");
	}
}
